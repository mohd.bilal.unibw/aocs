import numpy as np

def x_tilde(x):
    return np.array([
        [0,    -x[2],  x[1]],
        [x[2],     0, -x[0]],
        [x[1],  x[0],     0]
    ])