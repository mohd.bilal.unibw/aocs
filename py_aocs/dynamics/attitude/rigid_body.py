import numpy as np
from core.spacecraft.dynamics.attitude import x_tilde

def attitude_rate(q, w, T_ext, I):
    """Calculates the rate of attitude state vector.
    An attitude state is defined by quaternion, q and
    rotation rate w.

    Parameters
    ----------
    q : np.ndarray
        quaternion of the form (q0, q_v) where
        q0 is the scalar part and q_v is the vector part.
        q is a (4,) vector of the form, [q0, q1, q2, q3], s.t.,
        q_v = [q1, q2, q3]
    
    w : np.ndarray
        current rotation rate along 3 body axes.
    
    T_ext : np.ndarray
        external torque acting on the body in all 3 axes.
        Must be expressed body frame

    I : np.ndarray
        inertia tensor of shape (3, 3)
    
    Returns
    -------
    qdot, wdot : tuple
        tuple containing rates of quaternions and angular acceleration.

    Note
    ----
    All units must be SI units.

    Assumptions
    -----------
    * Inertia matrix is defined in body frame
    * All vectors are expressed in the same frame

    References
    ----------
    Eq. 3.103 : Schaub, Junkins. Analytical Mechanics of Space Systems 
    Eq. 4.32  : Schaub, Junkins. Analytical Mechanics of Space Systems
    """
    assert I.shape == (3, 3), "Inertia tensor must be 3x3 matrix."

    # calculate quaternion dynamics
    qdot = np.array([
        [0,    -w[0], -w[1], -w[2]],
        [w[0],     0,  w[2], -w[1]],
        [w[1], -w[2],     0,  w[0]],
        [w[2],  w[1], -w[0],     0]
    ])@q

    # calculate angular acceleration
    wdot = np.linalg.inv(I)@(-x_tilde(w)@I@w + T_ext)
    return qdot, wdot



