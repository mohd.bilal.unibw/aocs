import numpy as np

def triad(r1_b, r2_b, r1_n, r2_n, r_accurate=1):
    """TRIAD method for static attitude determination (attitude at the given instance of time).
    Static attitude measurement implies that we need to find attitude given 
    some observations in the body frame, e.g., sun vector and magnetic field vector.

    Parameters
    ----------
    r1_b : np.ndarray
        first observed unit direction in body frame (e.g., sun vector).
        Shape of r1_b should be (3,)

    r2_b : np.ndarray
        second observed unit direction in body frame (e.g., sun vector)

    r1_n : np.ndarray
        first unit direction in inertial frame (e.g., sun vector)

    r2_n : np.ndarray
        second unit direction in inertial frame (e.g., sun vector)

    Returns
    -------
    BN : np.ndarray
        attitude of body as DCM. array of shape (3, 3)
    """
    ## compute tranform TB, between a new frame and body frame
    
    # first direction is always the more accurate observation
    t1_b = r1_b

    # set the more accurate observation
    if r_accurate!=1:
        t1_b = r2_b

    # get second unit direction of T in B frame
    t2_b = np.cross(r1_b, r2_b)

    # the method fails if the two observations are colinear, i.e., cross is zero
    assert not np.all(t2_b==0.0), "The two observations should not be colinear."
    # make the vector unit
    t2_b = t2_b/np.linalg.norm(t2_b)
    
    # get third unit direction of T
    t3_b = np.cross(t1_b, t2_b)

    # get transform between T and B.
    TB = np.array([t1_b, t2_b, t3_b])

    ## Repeat the steps above for TN
    t1_n = r1_n
    if r_accurate!=1:
        t1_n = r2_n
    t2_n = np.cross(r1_n, r2_n)
    assert not np.all(t2_n==0), "The two observations should not be collinear."
    t2_n = t2_n/np.linalg.norm(t2_n)
    t3_n = np.cross(t1_n, t2_n)
    TN = np.array([t1_n, t2_n, t3_n])

    # calculate attitude as dcm
    BN = TB.T@TN

    return BN
    

def davenport_q(r_b, r_n, weights=None):
    """Implements Daventport's method of attitude determination using n observations.
    Given n observations in body frame and the same vectors in inertial frame (environment),
    Davenport's method finds the optimal quaternions.

    Parameters
    ----------
    r_b :  np.ndarray
        n observations stacked horizontally to make a (3, n) matrix

    r_n : np.ndarray
        n vectors in inertial frame stacked horizontally. shape is (3, n)

    weights : np.ndarray
        weighting for each observation (Note: weights are relative.)
    
    Returns
    -------
    q : np.ndarray
        attitude as quaternion. Shape (4, )

    NOTE:
    -----
    It must be noted that the function returns the quaternion as it is. Handling
    of shorter rotation (q0>0) or longer (q0<0) must be handled by the user. 
    This provides flexibility of usage.
    """
    n = r_b.shape[1]

    # we assume weights to be equal if not provided
    if weights is None:
        weights = np.ones((n,))
    
    # initialize B to zeros
    B = np.zeros((3,3))

    # for each observation, compute product and add to B
    for idx in range(n):
        w_idx = weights[idx]
        b_idx = w_idx*(r_b[:, [idx]]@(r_n[:, [idx]].T))
        B = B + b_idx
    
    # calculate S and K matrix
    S = B + B.T
    sigma = np.trace(B)
    Z = np.array([B[1,2] - B[2,1], B[2,0] - B[0,2], B[0,1] - B[1,0]])
    K_upper = np.hstack((sigma, Z.T))
    K_lower = np.hstack((Z.reshape((3,1)), S-sigma*np.eye(3, 3)))
    K = np.vstack((K_upper, K_lower))

    # get the eigen vector with the maximum eigen value. proved by Davenport
    eig_val, eig_vec = np.linalg.eig(K)
    max_ev_idx = np.argmax(eig_val)

    # the estimated attitude is the eigen vector with max eigen value
    q = eig_vec[:, max_ev_idx]
    return q


def quest(r_b, r_n, weights=None):
    """QUEST algorithm for attitude determination.
    Given n observations in body frame and the same vectors in inertial frame (environment),
    Davenport's method finds the optimal quaternions.

    Parameters
    ----------
    r_b :  np.ndarray
        n observations stacked horizontally to make a (3, n) matrix

    r_n : np.ndarray
        n vectors in inertial frame stacked horizontally. shape is (3, n)

    weights : np.ndarray
        weighting for each observation (Note: weights are relative.)
    
    Returns
    -------
    q : np.ndarray
        attitude as quaternion. Shape (4, )

    NOTE:
    -----
    It must be noted that the function returns the quaternion as it is. Handling
    of shorter rotation (q0>0) or longer (q0<0) must be handled by the user. 
    This provides flexibility of usage.
    """
    # get the number of observations
    n = r_b.shape[1]
    
    # assign equal weights if not provided by user
    if weights is None:
        weights = np.ones((n,))

    # create B matrix by adding contribution of each user
    B = np.zeros((3,3))
    for idx in range(n):
        w_idx = weights[idx]
        b_idx = w_idx*(r_b[:, [idx]]@(r_n[:, [idx]].T))
        B = B + b_idx
    
    # compute S matrix
    S = B + B.T
    sigma = np.trace(B)
    Z = np.array([B[1,2] - B[2,1], B[2,0] - B[0,2], B[0,1] - B[1,0]])

    # get K matrix
    K_upper = np.hstack((sigma, Z))
    K_lower = np.hstack((Z.reshape((3,1)), S-sigma*np.eye(3, 3)))
    
    # calculate the optimal eigen value by Newton Raphson
    K = np.vstack((K_upper, K_lower))
    f = lambda s : np.linalg.det(K - s*np.eye(4))

    def df(lambda_opt, dh=0):
        if not dh:
            dh = lambda_opt/10
        fp = f(lambda_opt + dh/2)
        fn = f(lambda_opt - dh/2)
        return (fp - fn)/dh
    
    # The first guess for optimal eigen value is the sum of weights
    # this is shown by inventor of QUEST
    lambda_opt = np.sum(weights)
    for itr in range(4):
        lambda_opt =  lambda_opt - f(lambda_opt)/df(lambda_opt)
    X = (lambda_opt + sigma)*np.eye(3) - S

    # get the CRP (Classical Rodrigues' Parameters)
    crp = np.linalg.inv(X)@Z

    # Convert CRP to Quaternion
    # TODO: Write a function for this in attitude.parameters
    q = np.hstack((1, crp))/np.sqrt(1 + np.linalg.norm(crp)**2)
    return q


def olae():
    """Implements OLAE method for attitude determination.
    """
    pass