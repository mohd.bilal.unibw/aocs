import numpy as np


def quat2dcm(q):
    """Transforms quaternion to direction cosine matrix.

    Parameters
    ----------
    q : np.ndarray
        quaternion of the form (q0, q_v) where
        q0 is the scalar part and q_v is the vector part.
        q is a (4,) vector of the form, [q0, q1, q2, q3], s.t.,
        q_v = [q1, q2, q3]
    
    Returns
    -------
    C : np.array
        direction cosine matrix of shape (3,3)

    References
    ----------
    Eq. 3.92 : Schaub, Junkins. Analytical Mechanics of Space Systems
    """
    q0, q1, q2, q3 = q[0], q[1], q[2], q[3]
    C = np.array([
        [q0**2 + q1**2 -q2**2 - q3**2,             2*(q1*q2 + q0*q3),             2*(q1*q3 - q0*q2)],
        [           2*(q1*q2 - q0*q3), q0**2 - q1**2 + q2**2 - q3**2,             2*(q2*q3 + q0*q1)],
        [           2*(q1*q3 + q0*q2),             2*(q2*q3 - q0*q1), q0**2 - q1**2 - q2**2 + q3**2]
    ])
    return C


def dcm2quat(dcm):
    """Converts Direction Cosine Matrix to quaternion using Stanley's method.
    This function always returns the positive quaternion. The resolution of the
    sign of quaternion is to be managed by the users in their applications.

    Parameters
    ----------
    dcm : np.ndarray
        direction cosine matrix of shape(3, 3)
    
    Returns
    -------
    q : np.ndarray
        quaternion of shape (4,) s.t. q = [q0, q1, q2, q3]
        where q_scalar = q0, q_vec = [q1, q2, q3]

    References
    ----------
    Eq. 3.94 : Schaub, Junkins. Analytical Mechanics of Space Systems
    """
    assert dcm.shape==(3, 3), "Wrong shape. DCM should be of shape (3,3)."
    q0 = np.sqrt(0.25*(1 + np.trace(dcm)))
    q1 = np.sqrt(0.25*(1 + 2*dcm[0, 0] - np.trace(dcm)))
    q2 = np.sqrt(0.25*(1 + 2*dcm[1, 1] - np.trace(dcm)))
    q3 = np.sqrt(0.25*(1 + 2*dcm[2, 2] - np.trace(dcm)))
    
    # instantiate new q
    q0_, q1_, q2_, q3_ = 0.0, 0.0, 0.0, 0.0
    
    # get products of quaternions from dcm
    q0q1 = 0.25*(dcm[1,2] - dcm[2,1])
    q0q2 = 0.25*(dcm[2,0] - dcm[0,2])
    q0q3 = 0.25*(dcm[0,1] - dcm[1,0])
    q2q3 = 0.25*(dcm[1,2] + dcm[2,1])
    q3q1 = 0.25*(dcm[2,0] + dcm[0,2])
    q1q2 = 0.25*(dcm[0,1] + dcm[1,0])

    # find the maximum q_i
    max_q = np.argmax([q0, q1, q2, q3])
    if max_q == 0:
        q0_ = q0
        q1_ = q0q1/q0_
        q2_ = q0q2/q0_
        q3_ = q0q3/q0_
    elif max_q == 1:
        q1_ = q1
        q0_ = q0q1/q1_
        q2_ = q1q2/q1_
        q3_ = q3q1/q1_
    elif max_q ==2:
        q2_ = q2
        q0_ = q0q2/q2_
        q1_ = q1q2/q2_
        q3_ = q2q3/q2_
    else:
        q3_ = q3
        q0_ = q0q3/q3_
        q1_ = q3q1/q3_
        q2_ = q2q3/q3_
    return np.array([q0_, q1_, q2_, q3_])


def euler_1(theta):
    """Computes DCM for Euler angle rotation about first Euler axis.

    Parameters
    ----------
    theta : float
        angle of rotation in radians

    Returns
    -------
    dcm : np.ndarray
        DCM array of shape (3,3)
    """
    return np.array([
        [1,              0,             0],
        [0,  np.cos(theta), np.sin(theta)],
        [0, -np.sin(theta), np.cos(theta)]
    ])


def euler_2(theta):
    """Computes DCM for Euler angle rotation about second Euler axis.

    Parameters
    ----------
    theta : float
        angle of rotation in radians

    Returns
    -------
    dcm : np.ndarray
        DCM array of shape (3,3)
    """
    return np.array([
        [np.cos(theta), 0, -np.sin(theta)],
        [0,             1,              0],
        [np.sin(theta), 0,  np.cos(theta)]
    ])


def euler_3(theta):
    """Computes DCM for Euler angle rotation about third Euler axis.

    Parameters
    ----------
    theta : float
        angle of rotation in radians

    Returns
    -------
    dcm : np.ndarray
        DCM array of shape (3,3)
    """
    return np.array([
        [ np.cos(theta), np.sin(theta), 0],
        [-np.sin(theta), np.cos(theta), 0],
        [             0,             0, 1]
    ])


def tr_dcm_bn(b1_n, b2_n, b3_n):
    """DCM for transform between B frame and N frame.
    The function accepts the three axes of B frame expressed in N frame.
    All coordinate frames are assumed to be orthogonal.

    Parameters
    ----------
    b1_n : np.ndarray
        Axis 1 of B-frame expressed in N-frame coordinates

    b2_n : np.ndarray
            Axis 2 of B-frame expressed in N-frame coordinates

    b3_n : np.ndarray
        Axis 3 of B-frame expressed in N-frame coordinates

    Returns
    -------
    dcm : np.ndarray
        Direction cosine matrix [BN], read as N to B. It can transforms
        vectors defined in N-frame to B-frame by pre-multiplication.
    """
    return np.array([
        b1_n,
        b2_n,
        b3_n
    ])