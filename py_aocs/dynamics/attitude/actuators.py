import numpy as np
from core.spacecraft.dynamics.attitude import x_tilde


def vscmg(w, T_ext, I_s, I_g, I_w):
    """Models the dynamics of a Variable Speed Control Moment Gyro (VSCMG).
    A VSCMG has the gimbal axis aligned with one of the axes of the spacecraft
    body frame. A wheel is gimbaled (or rotated) about this axis (called gimbal axis). 

    The frame of the gimbal is defined (observed and expressed) in the 
    spacecraft body coordinate frame.
    
    Parameters
    ----------
    gamma_0 : float
        initial gimbal angle
    """
    J = I_g + I_w
    I = I_s + J

    Iwdot = -x_tilde(w)@I@w \
        - g_s@(J_s@(J_s@(Omega_dot + gamma_dot@w_t) -(J_t - J_g)@w_t@gamma_dot))\
        - g_t@(J_s@(w_s + Omega)@gamma_dot          -(J_t + J_g)@w_s@gamma_dot + J_s@Omega@w_g)\
        - g_g@(J_g@gamma_dotdot - J_s@Omega@w_t)\
        + T_ext
    
    wdot = np.linalg.inv(I)@Iwdot
    return wdot