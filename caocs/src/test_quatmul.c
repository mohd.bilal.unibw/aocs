#include <math.h>
#include <stdio.h>
#include <parameters.h>

int main()
{
    quat q1 = {0.0, sqrt(1.0 / 2.0), sqrt(1.0 / 2.0), 0.0};
    quat q2 = {0.5 * sqrt(sqrt(3.0) / 2.0 + 1.0), -0.5 * sqrt(sqrt(3.0) / 2.0 + 1.0), -0.25 * sqrt(2.0) / (sqrt(2.0 + sqrt(3.0))), 0.25 * sqrt(2.0) / (sqrt(2.0 + sqrt(3.0)))};
    quat q;
    multiply_quat(q1, q2, q);

    for (int i = 0; i < 4; i++)
    {
        printf("q[%d]=%.5f \n", i, q[i]);
    }
}