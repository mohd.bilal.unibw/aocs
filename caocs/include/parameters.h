#ifndef PARAMETERS_H_

#define PARAMETERS_H_

typedef float quat[4];

void multiply_quat(quat q1, quat q2, quat q);

#endif